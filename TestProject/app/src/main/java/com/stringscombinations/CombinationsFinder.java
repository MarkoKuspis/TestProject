package com.stringscombinations;

import java.util.ArrayList;
import java.util.List;

/**
 * Class which provides functionality to find possible String tokens combinations which would
 * match target word.
 */
public class CombinationsFinder {

    private final ArrayList<StringsCombination> combinations;
    private final ArrayList<String> dictionary;

    /**
     * Constructs CombinationFinder.
     */
    public CombinationsFinder() {
        combinations = new ArrayList<>();
        dictionary = new ArrayList<>();
    }

    /**
     * Returns list of possible tokens combinations which match target word.
     *
     * @param dictionary dictionary to get tokens from.
     * @param target     word to check for matching.
     * @return combinations list.
     */
    public ArrayList<StringsCombination> findCombinations(final List<String> dictionary,
                                                          final String target) {
        combinations.clear();
        this.dictionary.clear();
        this.dictionary.addAll(filterDictionary(dictionary, target));

        findCombinations(new StringsCombination(), target);

        return combinations;
    }

    private ArrayList<String> filterDictionary(final List<String> dictionary,
                                               final String target) {
        final ArrayList<String> filteredDictionary = new ArrayList<>();

        for (final String token : dictionary) {
            if (target.contains(token)) {
                filteredDictionary.add(token);
            }
        }

        return filteredDictionary;
    }

    private void findCombinations(final StringsCombination combination,
                                  final String target) {
        if (target.isEmpty()) {
            combinations.add(new StringsCombination(combination));
            return;
        }

        for (final String token : dictionary) {
            if (target.startsWith(token)) {
                combination.addToken(token);

                findCombinations(combination, target.substring(token.length()));

                combination.removeToken();
            }
        }
    }
}