package com.stringscombinations;

import java.util.LinkedList;

/**
 * Class that contains array of Strings which can be combined into single string.
 */
public class StringsCombination {

    private final LinkedList<String> tokens;

    /**
     * Creates StringCombination instance with empty tokens.
     */
    public StringsCombination() {
        tokens = new LinkedList<>();
    }

    /**
     * Creates StringCombination by copying other combination.
     *
     * @param combination combination to copy tokens from.
     */
    public StringsCombination(final StringsCombination combination) {
        tokens = new LinkedList<>(combination.tokens);
    }

    /**
     * Adds token to the end of combination.
     *
     * @param token string token to add.
     */
    public void addToken(final String token) {
        tokens.add(token);
    }

    /**
     * Removes last token from combination.
     */
    public void removeToken() {
        tokens.removeLast();
    }

    private String combineToString() {
        final StringBuilder builder = new StringBuilder();

        for (final String token : tokens) {
            builder.append(token);
        }

        return builder.toString();
    }

    /**
     * Returns true if given string is equal to tokens combined into single string.
     *
     * @param targetString string to compare to
     * @return true if given string is equal to tokens combined into single string.
     */
    public boolean isMatchingString(final String targetString) {
        return combineToString().equals(targetString);
    }

    /**
     * Returns String which is sequence of tokens separated by space.
     *
     * @return String which is sequence of tokens separated by space.
     */
    public String formatTokens() {
        final StringBuilder builder = new StringBuilder();

        for (int i = 0; i < tokens.size(); i++) {
            builder.append(tokens.get(i));

            if (i < tokens.size() - 1) {
                builder.append(" ");
            }
        }

        return builder.toString();
    }
}