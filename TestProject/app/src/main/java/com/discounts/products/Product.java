package com.discounts.products;

/**
 * Describes product that can be bought and defines possible products.
 */
public class Product {

    private final String productId;
    private double price;

    /**
     * Creates Product instance.
     *
     * @param productId product identifier.
     * @param price     product price.
     */
    public Product(final String productId, final double price) {
        this.productId = productId;
        this.price = price;
    }

    /**
     * Sets product price.
     *
     * @param price new product price.
     */
    public void setPrice(final double price) {
        this.price = price;
    }

    /**
     * Returns product price.
     *
     * @return product price.
     */
    public double getPrice() {
        return price;
    }

    /**
     * Returns product identifier.
     *
     * @return product identifier.
     */
    public String getProductId() {
        return productId;
    }
}