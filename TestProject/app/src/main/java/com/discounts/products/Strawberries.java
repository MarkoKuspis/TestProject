package com.discounts.products;

/**
 * Defines Strawberries product.
 */
public class Strawberries extends Product {

    public static final String ID = "SR";
    public static final double PRICE = 5;

    /**
     * Creates Strawberries instance.
     */
    public Strawberries() {
        super(ID, PRICE);
    }
}