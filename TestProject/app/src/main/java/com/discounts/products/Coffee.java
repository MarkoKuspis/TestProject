package com.discounts.products;

/**
 * Defines Coffee product.
 */
public class Coffee extends Product {

    public static final String ID = "CF";
    public static final double PRICE = 11.23;

    /**
     * Creates Coffee instance.
     */
    public Coffee() {
        super(ID, PRICE);
    }
}