package com.discounts.products;

/**
 * Defines AppleJuice product.
 */
public class AppleJuice extends Product {

    public static final String ID = "AJ";
    public static final double PRICE = 7.25;

    /**
     * Creates AppleJuice instance.
     */
    public AppleJuice() {
        super(ID, PRICE);
    }
}