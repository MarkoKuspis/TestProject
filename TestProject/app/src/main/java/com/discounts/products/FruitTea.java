package com.discounts.products;

/**
 * Defines FruitTea product
 */
public class FruitTea extends Product {

    public static final String ID = "FR";
    public static final double PRICE = 3.11;

    /**
     * Creates FruitTea instance.
     */
    public FruitTea() {
        super(ID, PRICE);
    }
}