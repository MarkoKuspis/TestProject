package com.discounts.rule;

import com.discounts.products.Product;
import com.discounts.rule.appliers.DiscountApplier;
import com.discounts.rule.collectors.DiscountProductsCollector;

import java.util.List;

/**
 * Interface for discount rules that can be applied to products. Rule consist of
 * discountProductsCollector that determines list of products that should receive discount and
 * discount calculator which determines actual discount value for this items.
 */
public class Rule {

    private final DiscountProductsCollector discountProductsCollector;
    private final DiscountApplier discountApplier;

    /**
     * Creates new discount rule.
     *
     * @param discountProductsCollector needed to get list of products that satisfy condition.
     * @param discountApplier           needed to apply discount to list of products.
     */
    public Rule(final DiscountProductsCollector discountProductsCollector,
                final DiscountApplier discountApplier) {
        this.discountProductsCollector = discountProductsCollector;
        this.discountApplier = discountApplier;
    }

    /**
     * Applies discount to products. Criteria for determining which of products should receive
     * discount is described by discountProductsCollector. Actual discount that should be applied is
     * described by discountApplier.
     *
     * @param products products to buy
     */
    public void applyDiscount(final List<Product> products) {
        final List<Product> discountProducts = discountProductsCollector
                .getDiscountProducts(products);

        discountApplier.apply(discountProducts);
    }
}