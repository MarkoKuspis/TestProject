package com.discounts.rule.appliers;

import com.discounts.products.Product;

import java.util.List;

/**
 * Interface which defines method for applying discount.
 */
public interface DiscountApplier {

    /**
     * Applies discount to given products.
     *
     * @param products product list.
     */
    void apply(final List<Product> products);
}