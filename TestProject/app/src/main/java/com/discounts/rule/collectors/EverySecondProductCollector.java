package com.discounts.rule.collectors;

import com.discounts.products.Product;

import java.util.ArrayList;
import java.util.List;

/**
 * Defines product DiscountProductsCollector which collects only some portion of products.
 */
public class EverySecondProductCollector implements DiscountProductsCollector {

    private final String productId;

    /**
     * Creates new EverySecondProductCollector.
     *
     * @param productId ID of the product to search.
     */
    public EverySecondProductCollector(final String productId) {
        this.productId = productId;
    }

    @Override
    public List<Product> getDiscountProducts(final List<Product> products) {
        final List<Product> matchingProducts = getMatchingProducts(products);

        final int discountProductsNumber = matchingProducts.size() / 2;

        return matchingProducts.subList(0, discountProductsNumber);
    }

    private List<Product> getMatchingProducts(final List<Product> products) {
        final List<Product> matchingProducts = new ArrayList<>();
        for (final Product product : products) {
            if (product.getProductId().equals(productId)) {
                matchingProducts.add(product);
            }
        }

        return matchingProducts;
    }
}