package com.discounts.rule.appliers;

import com.discounts.products.Product;

import java.util.List;

/**
 * DiscountApplier which reduces product price by percentage from its previous price.
 */
public class ReducePriceByPercentageDiscountApplier implements DiscountApplier {

    private final double discountCoefficient;

    /**
     * Creates new ReducePriceByPercentageDiscountApplier with given discountCoefficient.
     *
     * @param discountCoefficient value to calculate new price for each product.
     */
    public ReducePriceByPercentageDiscountApplier(final double discountCoefficient) {
        this.discountCoefficient = discountCoefficient;
    }

    @Override
    public void apply(final List<Product> products) {
        for (final Product product : products) {
            product.setPrice(product.getPrice() - (product.getPrice() * discountCoefficient));
        }
    }
}