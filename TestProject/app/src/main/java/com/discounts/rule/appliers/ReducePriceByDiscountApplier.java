package com.discounts.rule.appliers;

import com.discounts.products.Product;

import java.util.List;

/**
 * DiscountApplier which describes discount which reduces price for each given product by
 * specified value. If product costs less than discount value it (product) is considered free.
 */
public class ReducePriceByDiscountApplier implements DiscountApplier {

    private final double reduceByValue;

    /**
     * Creates DiscountApplier with specified reduce price value.
     *
     * @param reduceByValue value to reduce product price by.
     */
    public ReducePriceByDiscountApplier(final double reduceByValue) {
        this.reduceByValue = reduceByValue;
    }

    @Override
    public void apply(final List<Product> products) {
        for (final Product product : products) {
            if (product.getPrice() > reduceByValue) {
                product.setPrice(product.getPrice() - reduceByValue);
            } else {
                product.setPrice(0);
            }
        }
    }
}