package com.discounts.rule.appliers;

import com.discounts.products.Product;

import java.util.List;

/**
 * DiscountApplier which describes type of discount when product price is set to particular value.
 * If product current price is lower than given price, product remains unchanged.
 */
public class SetPriceDiscountApplier implements DiscountApplier {

    private final double price;

    /**
     * Creates new SetPriceDiscountApplier.
     *
     * @param price value to set products price.
     */
    public SetPriceDiscountApplier(final double price) {
        this.price = price;
    }

    @Override
    public void apply(final List<Product> products) {
        for (final Product product : products) {
            if (price < product.getPrice()) {
                product.setPrice(price);
            }
        }
    }
}