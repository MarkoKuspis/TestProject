package com.discounts.rule.collectors;

import com.discounts.products.Product;

import java.util.ArrayList;
import java.util.List;

/**
 * Class which describes DiscountProductsCollector which returns list of more than given number
 * of particular Products.
 */
public class MoreThanProductsCollector implements DiscountProductsCollector {

    private final String productId;
    private final int productsNumber;

    /**
     * Creates new MoreThanProductsCollector.
     *
     * @param productId      ID of the product to search.
     * @param productsNumber minimum number of the products that should be found to apply discount.
     */
    public MoreThanProductsCollector(final String productId, final int productsNumber) {
        this.productId = productId;
        this.productsNumber = productsNumber;
    }

    @Override
    public List<Product> getDiscountProducts(final List<Product> products) {
        final List<Product> discountProducts = new ArrayList<>();

        for (final Product product : products) {
            if (product.getProductId().equals(productId)) {
                discountProducts.add(product);
            }
        }

        if (discountProducts.size() >= productsNumber) {
            return discountProducts;
        }

        return null;
    }
}