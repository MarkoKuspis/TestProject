package com.discounts.rule.collectors;

import com.discounts.products.Product;

import java.util.List;

/**
 * Interface which defines logic for determining which products should receive discount.
 */
public interface DiscountProductsCollector {

    /**
     * Returns list of products that should receive discount.
     *
     * @param products products to buy.
     * @return list of products that should receive discount.
     */
    List<Product> getDiscountProducts(final List<Product> products);
}