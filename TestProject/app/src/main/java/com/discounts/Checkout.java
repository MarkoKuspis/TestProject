package com.discounts;

import com.discounts.products.Product;
import com.discounts.rule.Rule;

import java.util.ArrayList;
import java.util.List;

/**
 * Class which describes checkout mechanism for scanning products and calculating price.
 */
public class Checkout {

    private final Rule[] rules;

    private final List<Product> products;

    /**
     * Creates Checkout instance which applies list of given discount rules for products.
     *
     * @param rules discount rules to apply to scanned products.
     */
    public Checkout(final Rule... rules) {
        this.rules = rules;

        products = new ArrayList<>();
    }

    /**
     * Scans product and adds it to product list.
     *
     * @param product product to buy.
     */
    public void scan(final Product product) {
        products.add(product);
    }

    /**
     * Returns price which includes currently working discounts.
     *
     * @return total price.
     */
    public double total() {
        for (final Rule rule : rules) {
            rule.applyDiscount(products);
        }

        double price = 0;
        for (final Product product : products) {
            price += product.getPrice();
        }

        return price;
    }

    /**
     * Resets scanned products list.
     */
    public void reset() {
        products.clear();
    }
}