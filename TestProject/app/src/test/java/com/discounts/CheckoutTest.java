package com.discounts;

import com.discounts.products.AppleJuice;
import com.discounts.products.Coffee;
import com.discounts.products.FruitTea;
import com.discounts.products.Strawberries;
import com.discounts.rule.Rule;
import com.discounts.rule.appliers.DiscountApplier;
import com.discounts.rule.appliers.ReducePriceByPercentageDiscountApplier;
import com.discounts.rule.appliers.SetPriceDiscountApplier;
import com.discounts.rule.collectors.DiscountProductsCollector;
import com.discounts.rule.collectors.EverySecondProductCollector;
import com.discounts.rule.collectors.MoreThanProductsCollector;

import junit.framework.Assert;

import org.junit.Test;

/**
 * Test suit for testing Checkout mechanism.
 */
public class CheckoutTest {

    @Test
    public void test0() {
        final DiscountProductsCollector collector = new EverySecondProductCollector(FruitTea.ID);
        final DiscountApplier applier = new ReducePriceByPercentageDiscountApplier(1);
        final Rule rule = new Rule(collector, applier);

        final Checkout checkout = new Checkout(rule);

        checkout.scan(new FruitTea());
        checkout.scan(new FruitTea());
        checkout.scan(new FruitTea());
        checkout.scan(new FruitTea());
        checkout.scan(new FruitTea());

        final double total = checkout.total();

        Assert.assertEquals(total, FruitTea.PRICE * 3, 0.1);
    }

    @Test
    public void test1() {
        final DiscountProductsCollector firstCollector =
                new EverySecondProductCollector(FruitTea.ID);
        final DiscountApplier firstApplier = new ReducePriceByPercentageDiscountApplier(1);
        final Rule firstRule = new Rule(firstCollector, firstApplier);

        final DiscountProductsCollector secondCollector = new MoreThanProductsCollector(
                Strawberries.ID, 3);
        final DiscountApplier secondApplier = new SetPriceDiscountApplier(4.5);
        final Rule secondRule = new Rule(secondCollector, secondApplier);

        final Checkout checkout = new Checkout(firstRule, secondRule);

        checkout.scan(new FruitTea());
        checkout.scan(new FruitTea());
        checkout.scan(new Strawberries());
        checkout.scan(new FruitTea());
        checkout.scan(new Strawberries());
        checkout.scan(new Strawberries());
        checkout.scan(new FruitTea());
        checkout.scan(new FruitTea());
        checkout.scan(new FruitTea());
        checkout.scan(new AppleJuice());
        checkout.scan(new Coffee());
        checkout.scan(new Coffee());

        final double expectedPrice = Coffee.PRICE * 2 + AppleJuice.PRICE + (FruitTea.PRICE * 3)
                + (4.5 * 3);

        final double total = checkout.total();

        Assert.assertEquals(expectedPrice, total, 0.01);
    }
}