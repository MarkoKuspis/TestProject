package com.stringscombinations;


import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * Test suit for {@link CombinationsFinder}.
 */
public class CombinationFinderTest {

    private static final String[] DICTIONARY = {"a", "b", "c", "ab", "abc"};

    @Test
    public void test0() {
        final CombinationsFinder finder = new CombinationsFinder();

        final ArrayList<StringsCombination> combinations = finder
                .findCombinations(Arrays.asList(DICTIONARY), "test");

        Assert.assertTrue(combinations.isEmpty());
    }

    @Test
    public void test1() {
        final CombinationsFinder finder = new CombinationsFinder();

        final ArrayList<StringsCombination> combinations = finder
                .findCombinations(Arrays.asList(DICTIONARY), "a");

        Assert.assertEquals(1, combinations.size());
        Assert.assertTrue(combinations.get(0).isMatchingString("a"));
    }

    @Test
    public void test2() {
        final CombinationsFinder finder = new CombinationsFinder();

        final ArrayList<StringsCombination> combinations = finder
                .findCombinations(Arrays.asList(DICTIONARY), "aabc");

        Assert.assertEquals(3, combinations.size());
        Assert.assertEquals(combinations.get(0).formatTokens(), "a a b c");
        Assert.assertEquals(combinations.get(1).formatTokens(), "a ab c");
        Assert.assertEquals(combinations.get(2).formatTokens(), "a abc");
    }
}