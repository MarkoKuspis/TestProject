This project contains two task results:
    1. Algorithms task (TestProject/app/src/main/java/com/stringscombinations - source folder, 
TestProject/app/src/test/java/com/stringscombinations - tests folder)
    2. Discount task (TestProject/app/src/main/java/com/discounts - source folder,
TestProject/app/src/test/java/com/discount - tests folder)

This is a android Gradle-based project, so in order to run it you need import it 
into Android Studio IDE. 

After importing project you will need to complete following steps from Android 
Tools Project Site guide (http://tools.android.com/tech-docs/unit-testing-support):

    1. Set "Unit tests" build varian ("Setting up Android Studio" section, step 4)
    2. Run particular test suit ("Setting up Android Studio" section, step 7)